DROP TABLE IF EXISTS account_location_dt;
DROP TABLE IF EXISTS opportunity_date_dt;
DROP TABLE IF EXISTS account_type_dt;
DROP TABLE IF EXISTS involvement_ft;

CREATE TABLE account_location_dt(
    account_location_id int,
    account_city string,
    account_state string,
    account_country string
)
row format delimited fields terminated by ','
tblproperties("skip.header.line.count"="1");

CREATE TABLE opportunity_date_dt(
    opportunity_date_id int,
    opportunity_day int,
    opportunity_dow int,
    opportunity_month int,
    opportunity_year int
)
row format delimited fields terminated by ','
tblproperties("skip.header.line.count"="1");

CREATE TABLE account_type_dt(
    account_type_id int,
    account_type string
)
row format delimited fields terminated by ','
tblproperties("skip.header.line.count"="1");

CREATE TABLE involvement_ft(
    opportunities_count int,
    is_account_major_donor boolean,
    is_opportunity_anonymous boolean,
    donation_amount int,
    account_location_id int,
    campaign_id int,
    opportunity_date_id int,
    payment_frequency_id int,
    payment_method_id int,
    stage_name_id int,
    account_registration_date_id int,
    account_type_id int
)
row format delimited fields terminated by ','
tblproperties("skip.header.line.count"="1");

