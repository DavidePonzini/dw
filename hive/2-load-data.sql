LOAD DATA LOCAL INPATH '/home/student/share/dw/export/account_location_dt.csv'
    OVERWRITE INTO TABLE account_location_dt;

LOAD DATA LOCAL INPATH '/home/student/share/dw/export/opportunity_date_dt.csv'
    OVERWRITE INTO TABLE opportunity_date_dt;

LOAD DATA LOCAL INPATH '/home/student/share/dw/export/account_type_dt.csv'
    OVERWRITE INTO TABLE account_type_dt;

LOAD DATA LOCAL INPATH '/home/student/share/dw/export/involvement_ft.csv'
    OVERWRITE INTO TABLE involvement_ft;
