select
    opportunity_dow dow,
    opportunity_year year,
    opportunity_month,
    sum(donation_amount*opportunities_count) income
from
    involvement_ft
    join opportunity_date_dt
        on opportunity_date_dt.opportunity_date_id = involvement_ft.opportunity_date_id
group by
    opportunity_dow, opportunity_year, opportunity_month
    grouping sets(
        (opportunity_year, opportunity_month, opportunity_dow),
        (opportunity_year, opportunity_dow),
        (opportunity_month, opportunity_dow),
        (opportunity_dow),
        ()
    )
order by
    opportunity_dow,
    opportunity_year,
    opportunity_month
;
