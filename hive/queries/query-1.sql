select distinct
    *
from (
    select 
        opportunity_month,
        opportunity_year,
        sum(donation_amount*opportunities_count)
            over (partition by opportunity_month, opportunity_year)
            as income,
        sum(donation_amount*opportunities_count)
            over (partition by opportunity_month, opportunity_year)
            /sum(donation_amount*opportunities_count)
                over (partition by opportunity_month)
            as income_month,
        sum(donation_amount*opportunities_count)
            over (partition by opportunity_month, opportunity_year)
            /sum(donation_amount*opportunities_count)
                over (partition by opportunity_year)
            as income_year,
        sum(donation_amount*opportunities_count)
            over (partition by opportunity_month, opportunity_year)
            /sum(donation_amount*opportunities_count)
                over ()
            as total_income
    from
        involvement_ft
        join opportunity_date_dt
                on opportunity_date_dt.opportunity_date_id = involvement_ft.opportunity_date_id
) t
order by opportunity_year, opportunity_month
;
