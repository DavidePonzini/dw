select 
    account_country as country,
    sum(donation_amount*opportunities_count) as income,
    dense_rank() over(order by sum(donation_amount*opportunities_count) desc) as rank
from
    involvement_ft
    join account_location_dt
        on involvement_ft.account_location_id = account_location_dt.account_location_id
group by
    account_country
;

