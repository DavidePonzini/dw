select 
    amount as income_month,
    sum(amount) over(
        order by opportunity_year, opportunity_month
        rows 3 preceding)
        as income_3_months,
    sum(amount) over(
        order by opportunity_year, opportunity_month
        rows 6 preceding)
        as income_6_months,
    opportunity_month as month,
    opportunity_year as year

from (
    select
        sum(donation_amount*opportunities_count) as amount,
        opportunity_month,
        opportunity_year
    from
        involvement_ft
        join account_location_dt
            on involvement_ft.account_location_id = account_location_dt.account_location_id
        join opportunity_date_dt
            on involvement_ft.opportunity_date_id = opportunity_date_dt.opportunity_date_id
    where
        account_country = 'USA'
    group by opportunity_year, opportunity_month
) as t
;

