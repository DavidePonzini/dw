select 
    sum(amount) over(
        order by opportunity_year
        rows unbounded preceding)
        as income_cum,
    opportunity_year as year

from (
    select
        sum(donation_amount*opportunities_count) as amount,
        opportunity_year
    from
        involvement_ft
        join account_location_dt
            on involvement_ft.account_location_id = account_location_dt.account_location_id
        join opportunity_date_dt
            on involvement_ft.opportunity_date_id = opportunity_date_dt.opportunity_date_id
    where
        account_country = 'USA'
    group by opportunity_year
) as t
;

