BEGIN;

CREATE SCHEMA involvement;

set search_path to "involvement";


CREATE TABLE account_location_dt
(
	account_location_id SERIAL PRIMARY KEY,
	
	account_city VARCHAR(50),
	account_state VARCHAR(50),
	account_country VARCHAR(50)
);

CREATE TABLE campaign_dt
(
	campaign_id SERIAL PRIMARY KEY,
	
	campaign VARCHAR(85)
);

CREATE TABLE opportunity_date_dt
(
	opportunity_date_id SERIAL PRIMARY KEY,
	
	opportunity_day INTEGER NOT NULL,
	opportunity_dow INTEGER NOT NULL,
	opportunity_month INTEGER NOT NULL,
	opportunity_year INTEGER NOT NULL
);

CREATE TABLE payment_frequency_dt
(
	payment_frequency_id SERIAL PRIMARY KEY,
	
	payment_frequency VARCHAR(50)
	
);

CREATE TABLE payment_method_dt
(
	payment_method_id SERIAL PRIMARY KEY,
	
	payment_method VARCHAR(50)
);

CREATE TABLE stage_name_dt
(
	stage_name_id SERIAL PRIMARY KEY,
	
	stage_name VARCHAR(50)
);

CREATE TABLE account_registration_date_dt
(
	account_registration_date_id SERIAL PRIMARY KEY,
	
	account_registration_day INTEGER NOT NULL,
	account_registration_month INTEGER NOT NULL,
	account_registration_year INTEGER NOT NULL
);

CREATE TABLE account_type_dt
(
	account_type_id SERIAL PRIMARY KEY,
	
	account_type VARCHAR(50)
);


CREATE TABLE involvement_ft
(
	opportunities_count NUMERIC(6,0),
	
	is_account_major_donor BOOLEAN,
	is_opportunity_anonymous BOOLEAN,
	donation_amount NUMERIC (12,2),
	
	account_location_id INTEGER NOT NULL,
	campaign_id INTEGER NOT NULL,
	opportunity_date_id INTEGER NOT NULL,
	payment_frequency_id INTEGER NOT NULL,
	payment_method_id INTEGER NOT NULL,
	stage_name_id INTEGER NOT NULL,
	account_registration_date_id INTEGER NOT NULL,
	account_type_id INTEGER NOT NULL,
	
	PRIMARY KEY(account_location_id,
		campaign_id,
		opportunity_date_id,
		payment_frequency_id,
		payment_method_id,
		stage_name_id,
		account_type_id,
		account_registration_date_id,
		is_account_major_donor,
		is_opportunity_anonymous,
		donation_amount),
	
	CONSTRAINT account_location_id FOREIGN KEY(account_location_id) REFERENCES account_location_dt(account_location_id),
	CONSTRAINT campaign_id FOREIGN KEY(campaign_id) REFERENCES campaign_dt(campaign_id),
	
	CONSTRAINT opportunity_date_id FOREIGN KEY(opportunity_date_id) REFERENCES opportunity_date_dt(opportunity_date_id),
	CONSTRAINT paymentFrequency FOREIGN KEY(payment_frequency_id) REFERENCES payment_frequency_dt(payment_frequency_id),
	CONSTRAINT paymentMethod FOREIGN KEY(payment_method_id) REFERENCES payment_method_dt(payment_method_id),
	
	CONSTRAINT stage_name_id FOREIGN KEY(stage_name_id) REFERENCES stage_name_dt(stage_name_id),
	CONSTRAINT account_registration_date_id FOREIGN KEY(account_registration_date_id) REFERENCES account_registration_date_dt(account_registration_date_id),
	CONSTRAINT account_type_id FOREIGN KEY(account_type_id) REFERENCES account_type_dt(account_type_id)
	
);

COMMIT;
