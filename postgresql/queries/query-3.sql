set search_path to involvement;

select 
	sum(amount) over(
		order by opportunity_year
		rows unbounded preceding)
		as income_cum,
--	opportunity_month as month,
	opportunity_year as year

from (
	select
		sum(donation_amount*opportunities_count) as amount,
		opportunity_year
	from
		involvement_ft
		natural join account_location_dt
		natural join opportunity_date_dt
	where
		account_country = 'USA'
	group by opportunity_year
) as t
;

