set search_path to involvement;

select distinct
	opportunity_month,
	opportunity_year,
	sum(donation_amount*opportunities_count)
		over (partition by opportunity_month, opportunity_year)
		as income,
	sum(donation_amount*opportunities_count)
		over (partition by opportunity_month, opportunity_year)
		/sum(donation_amount*opportunities_count)
			over (partition by opportunity_month)
		as income_month,
	sum(donation_amount*opportunities_count)
		over (partition by opportunity_month, opportunity_year)
		/sum(donation_amount*opportunities_count)
			over (partition by opportunity_year)
		as income_year,
	sum(donation_amount*opportunities_count)
		over (partition by opportunity_month, opportunity_year)
		/sum(donation_amount*opportunities_count)
			over ()
		as total_income
from
	involvement_ft
	natural join opportunity_date_dt
order by
	opportunity_year, opportunity_month
;

