#!/bin/bash -e

path=/home/student/share/dw/

rm -fv ${path}/data/export/*

psql dw < ${path}/postgresql/export.sql

# replace 'f' with 'false' and 't' with 'true'
sed -i 's/f,/false,/g' ${path}/data/export/involvement_ft.csv
sed -i 's/t,/true,/g' ${path}/data/export/involvement_ft.csv
