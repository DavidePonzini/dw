set search_path to involvement;

COPY involvement_ft      TO '/home/student/share/dw/data/export/involvement_ft.csv'      WITH (FORMAT CSV, HEADER);
COPY account_type_dt     TO '/home/student/share/dw/data/export/account_type_dt.csv'     WITH (FORMAT CSV, HEADER);
COPY account_location_dt TO '/home/student/share/dw/data/export/account_location_dt.csv' WITH (FORMAT CSV, HEADER);
COPY opportunity_date_dt TO '/home/student/share/dw/data/export/opportunity_date_dt.csv' WITH (FORMAT CSV, HEADER);
