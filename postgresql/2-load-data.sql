BEGIN;

set search_path to "involvement";

CREATE TEMPORARY TABLE temp
(
    AccountId 					CHAR(18),
	Id							CHAR(18),
	CampaignName 				VARCHAR(100),
	CloseDate_DOW 				NUMERIC(1),
	CloseDate_Day				NUMERIC(2),
	CloseDate_Month				NUMERIC(2),
	CloseDate_Year				NUMERIC(4),
	IsAnonymous					BOOLEAN,
	PaymentFrequency			VARCHAR(50),
	PaymentMethod				VARCHAR(50),
	StageName				 	VARCHAR(50),
	TotalAmount 				NUMERIC(10,2),
	Type 						VARCHAR(50),
	AccountCreatedDate_Day 		NUMERIC(2),
	AccountCreatedDate_Month 	NUMERIC(2),
	AccountCreatedDate_Year 	NUMERIC(4),
	IsMajorDonor 				BOOLEAN,
	BillingCity 				VARCHAR(50),
	BillingState 				VARCHAR(50),
	BillingCountry 				VARCHAR(50)
);

COPY temp FROM '/home/student/share/dw/data/data.csv' CSV HEADER DELIMITER ',';


INSERT INTO payment_frequency_dt(payment_frequency)
	SELECT DISTINCT PaymentFrequency FROM temp
	WHERE PaymentFrequency <> '';
INSERT INTO payment_method_dt(payment_method)
	SELECT DISTINCT PaymentMethod FROM temp
	WHERE PaymentMethod <> '';
INSERT INTO stage_name_dt(stage_name)
	SELECT DISTINCT StageName FROM temp
	WHERE StageName <> '';
INSERT INTO account_type_dt(account_type)
	SELECT DISTINCT Type FROM temp
	WHERE Type <> '';
INSERT INTO campaign_dt(campaign)
	SELECT DISTINCT CampaignName FROM temp
	WHERE CampaignName <> '';
INSERT INTO account_location_dt(account_city, account_country, account_state)
	SELECT DISTINCT BillingCity, BillingCountry, BillingState FROM temp;
INSERT INTO opportunity_date_dt(opportunity_day, opportunity_dow, opportunity_month, opportunity_year)
	SELECT DISTINCT CloseDate_Day, CloseDate_DOW, CloseDate_Month, CloseDate_Year FROM temp;
INSERT INTO account_registration_date_dt(account_registration_day, account_registration_month, account_registration_year)
	SELECT DISTINCT AccountCreatedDate_Day, AccountCreatedDate_Month, AccountCreatedDate_Year FROM temp;

INSERT INTO involvement_ft(
		opportunities_count,
		is_account_major_donor,
		is_opportunity_anonymous,
		donation_amount,
		account_location_id,
		campaign_id,
		opportunity_date_id,
		payment_frequency_id,
		payment_method_id,
		stage_name_id,
		account_registration_date_id,
		account_type_id
	)
	SELECT COUNT(*) AS c,
		IsMajorDonor,
		IsAnonymous,
		TotalAmount,
		account_location_id,
		campaign_id,
		opportunity_date_id,
		payment_frequency_id,
		payment_method_id,
		stage_name_id,
		account_registration_date_id,
		account_type_id
	FROM temp
		LEFT JOIN account_location_dt ON BillingCity = account_city AND BillingCountry = account_country AND BillingState = account_state
		LEFT JOIN campaign_dt ON CampaignName = campaign
		LEFT JOIN opportunity_date_dt ON
			opportunity_day = CloseDate_Day AND
			opportunity_dow = CloseDate_DOW AND
			opportunity_month = CloseDate_Month AND
			opportunity_year = CloseDate_Year
		LEFT JOIN payment_frequency_dt ON payment_frequency = PaymentFrequency
		LEFT JOIN payment_method_dt ON payment_method = PaymentMethod
		LEFT JOIN stage_name_dt ON stage_name = StageName
		LEFT JOIN account_registration_date_dt ON
			account_registration_day = AccountCreatedDate_Day AND
			account_registration_month = AccountCreatedDate_Month AND
			account_registration_year = AccountCreatedDate_Year
		LEFT JOIN account_type_dt ON account_type = Type
	GROUP BY IsMajorDonor,
		IsAnonymous,
		TotalAmount,
		account_location_id,
		campaign_id,
		opportunity_date_id,
		payment_frequency_id,
		payment_method_id,
		stage_name_id,
		account_registration_date_id,
		account_type_id
;

COMMIT;
