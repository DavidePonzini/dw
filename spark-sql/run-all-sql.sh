#!/bin/bash

rm -f out_query-sql/*

for f in `ls -v query-sql/query-*`; do
	./run.sh sql_input.py ${f} > out_${f}
done
