Query 14 cannot be executed on SparkSQL, since it doesn't support subquery aliasing.

Read more at:
https://issues.apache.org/jira/browse/SPARK-20962

=====================================================================================

Regarding its Python implementation, I am unable to write the corresponding dataframe operations,
since they appear to be very complex.

I provided some comments on a possible solution in `query-14.py`, just above the where clause.

If a simpler solution does exist, I did not manage to find it, even after researching on the web.
