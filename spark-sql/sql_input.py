import spark
import sys


spark.load_table('account_location_dt')
spark.load_table('account_registration_date_dt')
spark.load_table('account_type_dt')
spark.load_table('campaign_dt')
spark.load_table('involvement_ft')
spark.load_table('opportunity_date_dt')
spark.load_table('payment_frequency_dt')
spark.load_table('payment_method_dt')
spark.load_table('stage_name_dt')

files = sys.argv[1:]

for file in files:
	with open(file) as f:
		data = f.read()
#		print(data)

		spark.sql_query(data)

