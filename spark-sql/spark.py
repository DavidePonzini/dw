from pyspark.sql import SparkSession


spark = SparkSession.builder.appName("dw").getOrCreate()


def read_jdbc(tablename):
	return spark.read.format("jdbc")\
		.option("url", "jdbc:postgresql:dw")\
		.option("dbtable", 'involvement.{}'.format(tablename))\
		.option("user", "student")\
		.option("password", "foobar")\
		.load()


def load_table(tablename):
	table = read_jdbc(tablename)
	table.createOrReplaceTempView(tablename)

	return table


def sql_query(sql):
	query = spark.sql(sql)
	query.show()

