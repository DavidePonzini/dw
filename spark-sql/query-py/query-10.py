import spark
import sys

import pyspark.sql.functions as sf


# load data
involvement = spark.load_table('involvement_ft')
payment_method = spark.load_table('payment_method_dt')

# where
involvement = involvement.where(involvement.is_account_major_donor == True)

# join
result = involvement.join(payment_method, involvement.payment_method_id == payment_method.payment_method_id)

# group by
result = result.groupby('payment_method')

# aggregates
result = result.agg(sf.sum('opportunities_count').alias('c'))

# select
result = result.select(result.c, result.payment_method)

# order by
result = result.orderBy('c', ascending=False)

# show result
result.show()
