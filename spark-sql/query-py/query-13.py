import spark
import sys

import pyspark.sql.functions as sf


# load data
involvement = spark.load_table('involvement_ft')
account_registration_date = spark.load_table('account_registration_date_dt')

# join
result = involvement.join(account_registration_date, involvement.account_registration_date_id == account_registration_date.account_registration_date_id)

# computed columns
result = result.withColumn('_tmp1', result.donation_amount * result.opportunities_count)

# group by
result = result.groupby('account_registration_year')

# aggregates
result = result.agg(
	sf.sum('opportunities_count').alias('donations_number'),
	sf.sum('opportunities_count').alias('avg_income'),
	sf.sum('_tmp1').alias('total_income'))

# select
result = result.select(
	result.account_registration_year.alias('year'),
	result.donations_number,
	result.avg_income,
	result.total_income)

# order by
result = result.orderBy('year')

# show result
result.show()
