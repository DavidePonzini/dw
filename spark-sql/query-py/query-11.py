import spark
import sys

import pyspark.sql.functions as sf


# load data
involvement = spark.load_table('involvement_ft')
payment_method = spark.load_table('payment_method_dt')
account_location = spark.load_table('account_location_dt')

# join
result = involvement.join(payment_method, involvement.payment_method_id == payment_method.payment_method_id)
result = result.join(account_location, result.account_location_id == account_location.account_location_id)

# group by
result = result.rollup('payment_method', 'account_country')

# aggregates
result = result.agg(sf.sum('opportunities_count').alias('c'))

# select
result = result.select(result.payment_method, result.c, result.account_country)

# order by
result = result.orderBy(['payment_method', 'c'], ascending=[True, False])

# show result
result.show()
