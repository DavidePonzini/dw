import spark
import sys

import pyspark.sql.functions as sf
from pyspark.sql import Window

# load data
involvement = spark.load_table('involvement_ft')
account_location = spark.load_table('account_location_dt')
payment_method = spark.load_table('payment_method_dt')
opportunity_date = spark.load_table('opportunity_date_dt')

# join
tmp = involvement.join(account_location, involvement.account_location_id == account_location.account_location_id)
tmp = tmp.join(payment_method, tmp.payment_method_id == payment_method.payment_method_id)
tmp = tmp.join(opportunity_date, tmp.opportunity_date_id == opportunity_date.opportunity_date_id)

# group by
tmp = tmp.groupby('account_country', 'opportunity_year', 'payment_method')

tmp = tmp.agg(sf.sum('opportunities_count').alias('opportunities'))

# select
tmp = tmp.select(
	tmp.account_country.alias('country'),
	tmp.opportunity_year.alias('year'),
	tmp.payment_method.alias('method'),
	tmp.opportunities)

# order by
tmp = tmp.orderBy('year')


########################################

# where
'''
General idea is:
- `where` keeps only relevant rows
- `sf.max` computes the maximum number of opportunities for a given year/country
- `select` should make it so we return a single column
- `collect` should convert that column to a list
- `max` returns the maximum value in list

we then check the number of opportunities for the current value
'''

#result = tmp.where(tmp.opportunities = max(tmp.where(...).withColumn('x', sf.max('opportunities')).select(tmp.x).collect()))

result = tmp	# put here just to make the following code execute

# order by
result = result.orderBy(
	result.country,
	result.year,
	result.method)

# show result
result.show()
