import spark
import sys

import pyspark.sql.functions as sf
from pyspark.sql import Window

# define windows
window_year_type = Window.partitionBy('opportunity_year', 'account_type')
window_year = Window.partitionBy('opportunity_year')
window_type = Window.partitionBy('account_type')

# load data
involvement = spark.load_table('involvement_ft')
account_type = spark.load_table('account_type_dt')
opportunity_date = spark.load_table('opportunity_date_dt')

# join
result = involvement.join(account_type, involvement.account_type_id == account_type.account_type_id)
result = result.join(opportunity_date, result.opportunity_date_id == opportunity_date.opportunity_date_id)

# computed columns
result = result.withColumn('_tmp_totalincome', result.donation_amount * result.opportunities_count)
result = result.withColumn('_tmp_window_y_t', sf.sum('_tmp_totalincome').over(window_year_type))
result = result.withColumn('_tmp_window_y', sf.sum('_tmp_totalincome').over(window_year))
result = result.withColumn('_tmp_window_t', sf.sum('_tmp_totalincome').over(window_type))

result = result.withColumn('income_year', result._tmp_window_y_t / result._tmp_window_y)
result = result.withColumn('income_type', result._tmp_window_y_t / result._tmp_window_t)

# select
result = result.select(
	result.income_year,
	result.income_type,
	result.account_type,
	result.opportunity_year.alias('year')).distinct()

# order by
result = result.orderBy('account_type', 'year')

# show result
result.show()
