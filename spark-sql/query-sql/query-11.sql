select
	payment_method,
	sum(opportunities_count) c,
	account_country
from
	involvement_ft
	natural join payment_method_dt
	natural join account_location_dt
group by
	rollup(payment_method, account_country)
order by
	payment_method, c desc
