select 
	amount as income_month,
	sum(amount) over(
		order by opportunity_year, opportunity_month
		rows 3 preceding)
		as income_3_months,
	sum(amount) over(
		order by opportunity_year, opportunity_month
		rows 6 preceding)
		as income_6_months,
	opportunity_month as month,
	opportunity_year as year

from (
	select
		sum(donation_amount*opportunities_count) as amount,
		opportunity_month,
		opportunity_year
	from
		involvement_ft
		natural join account_location_dt
		natural join opportunity_date_dt
	where
		account_country = 'USA'
	group by opportunity_year, opportunity_month
) as t
