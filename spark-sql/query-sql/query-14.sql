create temporary view q14t
as
	select
		account_country as country,
		opportunity_year as year,
		payment_method as method,
		sum(opportunities_count) as opportunities
	from
		involvement_ft
		natural join account_location_dt
		natural join payment_method_dt
		natural join opportunity_date_dt
	group by
		account_country, opportunity_year, payment_method


