select 
	account_country as country,
	sum(donation_amount*opportunities_count) as income,
	dense_rank() over(order by sum(donation_amount*opportunities_count) desc) as rank
from
	involvement_ft
	natural join account_location_dt
group by
	account_country

