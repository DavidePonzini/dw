select
	account_registration_year
		as year,
	sum(opportunities_count)
		as donations_number,
	sum(donation_amount*opportunities_count)
		/sum(opportunities_count)
		as avg_income,
	sum(donation_amount*opportunities_count)
		as total_income
from
	involvement_ft
	natural join account_registration_date_dt
group by
	account_registration_year
order by
	year
