select
	sum(opportunities_count) as c,
	payment_method
from
	involvement_ft
	natural join payment_method_dt
where
	is_account_major_donor = true
group by
	payment_method
order by
	c desc
