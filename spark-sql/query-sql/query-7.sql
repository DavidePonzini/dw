select
    sum(donation_amount*opportunities_count) amount,
    sum(opportunities_count) c,
    account_type
from
    involvement_ft
    join account_type_dt
        on account_type_dt.account_type_id = involvement_ft.account_type_id
group by
    account_type
order by
    amount desc

