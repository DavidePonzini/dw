select
    count(*) c,
    account_state
from
    involvement_ft
    join account_location_dt
        on account_location_dt.account_location_id = involvement_ft.account_location_id
where
    account_country = 'USA'
    and is_account_major_donor = true
group by
    account_state
order by
    c desc

