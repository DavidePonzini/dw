select
    dense_rank() over(partition by account_country order by c desc) rank,
    c,
    account_country,
    account_state
from (
    select
        sum(opportunities_count) c,
        account_state,
        account_country
    from
        involvement_ft
        join account_location_dt
            on account_location_dt.account_location_id = involvement_ft.account_location_id
    group by
        account_country, account_state
    order by
        c desc
) t
