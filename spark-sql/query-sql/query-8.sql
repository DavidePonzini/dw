select
    account_country,
    account_state,
    account_city,
    sum(opportunities_count) c,
    sum(donation_amount*opportunities_count) income
from
    involvement_ft
    join account_location_dt
        on account_location_dt.account_location_id = involvement_ft.account_location_id
where
    payment_frequency_id = 3
group by account_country, account_state, account_city with rollup
