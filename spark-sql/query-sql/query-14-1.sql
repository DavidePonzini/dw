select
	*
from
	q14t as t
where
	opportunities >= ALL(
		select 
			max(opportunities)
		from
			q14t
		where
			t.year = year
			and t.country = country
	)
order by
	country, year, method

-- not supported:
-- https://issues.apache.org/jira/browse/SPARK-20962
