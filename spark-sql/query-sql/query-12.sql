select distinct
	sum(donation_amount*opportunities_count)
		over(partition by (opportunity_year, account_type))
		/sum(donation_amount*opportunities_count)
			over(partition by (opportunity_year))
		as income_year,
	sum(donation_amount*opportunities_count)
		over(partition by (opportunity_year, account_type))
		/sum(donation_amount*opportunities_count)
			over(partition by (account_type))
		as income_type,
	account_type,
	opportunity_year as year
from
	involvement_ft
	natural join account_type_dt
	natural join opportunity_date_dt
order by
	account_type,
	year
