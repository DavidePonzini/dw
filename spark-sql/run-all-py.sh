#!/bin/bash -e

rm -f out_query-py/*

for f in query-py/query-*; do
	./run.sh ${f} > out_${f}
done
