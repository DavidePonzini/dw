# Project developed by
- Davide Ponzini
- Samuele Daga

# Folder description
- data: csv files used by data warehouse
  - export: data warehouse tables exported from postgreSQL for Hive
- dataset:
  - Accounts/Contacts/Opportunities.xlsx: basic column metrics
- hive:
  - queries: queries executed by Hive
- spark-sql
  - query-py: SparkSQL queries as Dataframe operations
  - query-sql: SQL queries executed by SparkSQL
  - README.txt: information on problems encountered for query 14
  - run.sh: runs a job on Spark (used by other scripts)
  - run-all-py.sh: runs all queries in *query-py*. Output goes to *out_query-py*
  - run-all-sql.sh: runs all queries in *query-sql*. Output goes to *out_query-sql*
  - spark.py: methods for interacting with postgreSQL database
  - sql_input.py: reads a SQL file and executes it in Spark
- tableau: recipe used by Tableau, contains all charts
- trifacta: contains all *.wrangle* files used by Trifacta
